from __future__ import division
import random
import hashlib
import hmac
import pdb

def random_digits(num):
    range_start = 10**(num-1)
    range_end = 10**(num) - 1

    return random.randint(range_start, range_end)

class JustDice(object):
    def __init__(self, server_seed, client_seed):
        self.client_seed = client_seed
        self.server_seed = server_seed

    def lucky_number(self, server_seed, client_seed, nonce):
        hex_chars_to_use = 5
        nonce_str = str(nonce)

        key = nonce_str + ':' + server_seed + ':' + nonce_str
        raw = nonce_str + ':' + client_seed + ':' + nonce_str

        tmp_hmac = hmac.new(key, digestmod=hashlib.sha512)
        tmp_hmac.update(raw)

        hash = tmp_hmac.hexdigest()

        for idx, char in enumerate(hash):
            hex = hash[idx:idx + hex_chars_to_use]
            lucky = int(hex, 16)

            if lucky < 1000000:
                return lucky / 10000

    def generate_numbers(self, num_bets):
        numbers = {}
        for num in range(1, num_bets + 1):
            number = self.lucky_number(self.server_seed, self.client_seed, num)
            numbers[num] = number
        return numbers

    def roll(self, orientation, chance_to_win, lucky):
        if orientation == 'hi':
            if lucky < chance_to_win:
                return 1

        if orientation == 'lo':
            if lucky > (100 - chance_to_win - 0.0001):
                return 1

        return 0

    def run_bets(self, num_bets, chance_to_win, orientation='lo'):
        numbers = self.generate_numbers(num_bets)
        outcomes = []

        for number in numbers:
            lucky = numbers[number]

            outcome = self.roll(orientation, chance_to_win, lucky)
            outcomes.append(outcome)

        return outcomes

    def randomize_client_seed(self):
        self.client_seed = str(random_digits(24))

if __name__ == '__main__':
    # EXAMPLE USAGE

    seed_list = []
    NUM_BETS = 10
    SERVER_SEED = 'XFnoIh2AnOUw2KV0861yMwRTieXpD88kY7SqWt3AWMNyE3j2.ZdeXkJ2EppjU4PO'
    CLIENT_SEED = '196639128646866345148463'

    jd = JustDice(SERVER_SEED, CLIENT_SEED)
    jd.randomize_client_seed()
    seed_list.append(jd.client_seed)
    bets = jd.run_bets(NUM_BETS, 90)
    dice_rolls = jd.generate_numbers(NUM_BETS)
    print seed_list
    print bets
    print dice_rolls

