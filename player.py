from betSim import BetSimulator
from bankSim import AccountSimulator


WIN_PROBABILITY = 90
NUM_BETS = 10
ACCOUNT_SIZE = 1000000
BASE_BET = 10

seed_list = []
SERVER_SEED = 'XFnoIh2AnOUw2KV0861yMwRTieXpD88kY7SqWt3AWMNyE3j2.ZdeXkJ2EppjU4PO'
CLIENT_SEED = '196639128646866345148463'

bet_sim = BetSimulator(SERVER_SEED, CLIENT_SEED)
bet_sim.randomize_client_seed()
seed_list.append(bet_sim.client_seed)

bets = bet_sim.run_bets(NUM_BETS, WIN_PROBABILITY)
dice_rolls = bet_sim.generate_numbers(NUM_BETS)

account = AccountSimulator(bets)
account.init_account(ACCOUNT_SIZE, BASE_BET)
account.process_bets()

pdb.set_trace()

