from bet_simulator import BetSimulator
import pdb
import random

class AccountSimulator(object):
    def __init__(self, bets):
        self.bets = bets
        self.profit = 0
        
        self.wins_to_losses = [0, 0]
        self.win_tier = {}
        self.loss_tier = {}
        self.verified = False

    def init_account(self, account_size, base_bet):
        self.account = account_size
        self.base_bet = base_bet
        self.verified = self.verify_account(base_bet)

    def get_bet(self, loss_streak):
        if self.base_bet >= 1:
            bet = (self.base_bet)*(1-(10)**(loss_streak+1))/(1-(10))
        if self.base_bet < 1:
            bet = (self.base_bet)/(1-(10))
        return bet

    def process_account(self, difference):
        self.account = self.account + (difference)
        self.profit = self.profit + (difference)

    def process_tiers(self, win_streak, loss_streak):
        if win_streak > 0:
            if len(self.win_tier) < win_streak: self.win_tier[win_streak - 1] = 0
            self.win_tier[win_streak - 1] += 1
        if loss_streak > 0:
            if len(self.loss_tier) < loss_streak: self.loss_tier[loss_streak - 1] = 0
            self.loss_tier[loss_streak - 1] += 1

    def process_streaks(self, win_loss):
        if win_loss[0] > 0:
            self.wins_to_losses[0] += 1
        if win_loss[1] > 0:
            self.wins_to_losses[1] += 1

    def process_bets(self):
        win_streak = 0
        loss_streak = 0

        for value in self.bets:
            if self.verified:
                bet = self.get_bet(loss_streak)
                if value == 1:
                    difference = bet/10
                    win_streak += 1
                    loss_streak = 0
                
                if value == 0:
                    difference = -bet
                    loss_streak += 1
                    win_streak = 0
                    
                self.process_account(difference)
                self.process_tiers(win_streak, loss_streak)
                self.process_streaks((win_streak, loss_streak))
                self.verified = self.verify_account(bet)

            else:
                break

        self.show_stats()

    def verify_account(self, bet):
        if self.account < bet:
            return False
        return True

    def show_stats(self):
        print "TOTAL BETS: {}".format(len(self.bets))
        print "TOTAL MONEY: {}".format(self.account)
        print "PROFIT: {}".format(self.profit)
        print "WIN_TIERS {}\nLOSS_TIERS {}".format(self.win_tier, self.loss_tier)
        print "TOTAL WINS: {}\nTOTAL LOSSES: {}".format(self.wins_to_losses[0], self.wins_to_losses[1])
        #print "FAIL CATCH: {}-[{}%]".format(fails_cought, get_fails_cought_percent(total_losses, fails_cought))
        print "--------------------------------------"  

if __name__ == '__main__':
    seed_list = []
    NUM_BETS = 1000
    ACCOUNT_SIZE = 1000000
    BASE_BET = 10
    SERVER_SEED = 'XFnoIh2AnOUw2KV0861yMwRTieXpD88kY7SqWt3AWMNyE3j2.ZdeXkJ2EppjU4PO'
    CLIENT_SEED = '196639128646866345148463'

    bet_sim = BetSimulator(SERVER_SEED, CLIENT_SEED)
    bet_sim.randomize_client_seed()
    seed_list.append(bet_sim.client_seed)
    bets = bet_sim.run_bets(NUM_BETS, 90)
    dice_rolls = bet_sim.generate_numbers(NUM_BETS)
    

    account = AccountSimulator(bets)
    account.init_account(ACCOUNT_SIZE, BASE_BET)
    account.process_bets()


    pdb.set_trace()
